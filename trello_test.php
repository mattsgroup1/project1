<?php

require_once 'vendor/autoload.php';

// This code sample uses the 'Unirest' library:
// http://unirest.io/php.html
$headers = array(
  'Accept' => 'application/json'
);

/* Matt Bull Login info */

$query = array(
  'key' => 'e0e6f606a1a92b5f75a04a62404adb24',
  'token' => 'API_TOKEN'
);

/* lists the board info */

/*$response = Unirest\Request::get(
  'https://api.trello.com/1/boards/DWiQyCEG',
  $headers,
  $query
);*/

/* lists in the board */

/*$response = Unirest\Request::get(
  'https://api.trello.com/1/boards/DWiQyCEG/lists',
  $headers,
  $query
);*/

/* cards in the list */

$query['idList'] = '5fae47d39ed91a29718161a5';

$response = Unirest\Request::get(
  'https://api.trello.com/1/lists/5fae47d39ed91a29718161a5/cards',
  $headers,
  $query
);

/* Create a new card */

/* Update a card */


var_dump($response;