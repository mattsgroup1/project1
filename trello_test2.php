<?php

require_once 'vendor/autoload.php';

// This code sample uses the 'Unirest' library:
// http://unirest.io/php.html
$headers = array(
  'Accept' => 'application/json'
);

//create card on trello board
$query_create_card_1 = array(
  'key' => 'e0e6f606a1a92b5f75a04a62404adb24',
  'token' => 'API_TOKEN',
  'idList' => '5fae47d39ed91a29718161a5',
  'name' => 'Test Card1',
  'desc' => 'Test Description1',
  'pos' => 'bottom',
	);

$response_create_card_1 = Unirest\Request::post(
  'https://api.trello.com/1/cards',
  $headers,
  $query_create_card_1
);