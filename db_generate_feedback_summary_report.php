<?php

require_once 'vendor/autoload.php';

// This code sample uses the 'Unirest' library:
// http://unirest.io/php.html
$headers = array(
  'Accept' => 'application/json'
);

$user = "PubliSure";
$pass = "PASSWORD";

$i = 0;

/*
    m.customerid,

    and (DATEDIFF(DATE(NOW()),m.time_submitted) > 2 AND execution_Set != 2)
*/

$sql = "select
    a.company_name,
    sum(if(execution_Set IN (0,1), 1, 0)) as 'pending',
    sum(if(execution_Set = 2, 1, 0)) as 'feedback',
    m.time_submitted,
    DATEDIFF(DATE(NOW()),m.time_submitted) as 'days_diff',
    IF( DATEDIFF(DATE(NOW()),m.time_submitted) > 2 AND execution_Set != 2 , 'YES', 'NO') as 'is_over'
from
    publisure.mailsort m 
        left join publisure.accounts a on m.customerid=a.record_id
where
    date(time_submitted) between DATE(SUBDATE(DATE(NOW()), INTERVAL 1 WEEK)) AND DATE(NOW())
    and recall_item=0 
    and execution_set in (0,1,2)

group by
    m.customerid
order by
    date(time_submitted) asc,
    execution_set";

$output = "";

try {
    $dbh = new PDO('mysql:host=127.0.0.1;port=56001;dbname=publisure', $user, $pass);
    $query = $dbh->query($sql, PDO::FETCH_ASSOC);

    $row_count = $query->rowCount();

    if($row_count > 0) {

        $title = "Non Feedback (1 week) " . date('d/m/Y');
        $title.= "\nRows returned: " . $row_count;

        foreach($query as $row) {

            //print_r($row);

            //change dynamicly for csv format

            /*if($i==0) {
                $colcount = count(array_keys($row));
                foreach (array_keys($row) as  $ck=>$col) {
    $output.= $col; if( ($ck + 1) <  $colcount ) $output.= ",";
                }
            }

    $output.= "\n";

            $valcount = count(array_values($row));

            foreach (array_values($row) as $vk=>$val) {
    $output.=  $val; if( ($vk + 1) <  $valcount ) $output.=  ",";
            }*/

            $title.= "\n" . $row['company_name'] . " " . $row['pending'] . " ";
            if($row['is_over']=='YES') $title.= 'CHECK';

            //display in a fixed way
            $output.= "\n\n" . $row['company_name'] .
            "\npending:" . $row['pending'] .
            "\nfeedback:" .  $row['feedback'] .
            "\ntime_submitted:" . $row['time_submitted'] .
            "\nis_over:" . $row['is_over']; //means attention is needed

            $i++;
        }

        $dbh = null;

        //debug output
        print_r(array(
            "title" => $title,
            "output" => $output
        ));

        //create card on trello board
        /*$query_create_card_1 = array(
          'key' => 'e0e6f606a1a92b5f75a04a62404adb24',
          'token' => 'API_TOKEN',
          'idList' => '5fae47d39ed91a29718161a5',
          'name' => $title,
          'desc' => $output,
          'pos' => 'bottom',
        );

        //sent report code here
        $response_create_card_1 = Unirest\Request::post(
          'https://api.trello.com/1/cards',
          $headers,
          $query_create_card_1
        );*/

    }



} catch (PDOException $e) {
    print "Error!: " . $e->getMessage() . "<br/>";
    die();
}



?>